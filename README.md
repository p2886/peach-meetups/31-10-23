# peach-meetup on the 31st of October 2023

## Overview

This repo contains public code shared during Peach Payments Meetups sessions and is free to fork and use by anyone. 

It deploys the following resources:
- A custom VPC, ready with NATs, Internet Gateways and all required Routing Tables to ensure connectivity
- A lambda sitting behind an API gateway, with access to the below RDS instance
- An RDS instance with a default database named `peachmeetup`
- An EC2 instance as a jump box to the RDS instance, all setup with access

## Disclaimer

Please note that the deployment of the above resources will incur cost on your AWS environment which is the responsibility of the individual using this code base.

Also, the region being used is `af-south-1` but you are welcome to make re


## Requirements

Ensure you have the following installed:
- [https://www.terraform.io/](Terraform)
- [https://www.serverless.com/framework/docs/getting-started](Serverless)
- [https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#getting-started-install-instructions](AWS CLI)

## Local Setup

Before moving on to deploying, ensure you have the following setup on your environment.

### Setup AWS CLI

Please follow the steps:

- Create the .aws directory under your home directory `mkdir -p ~/.aws`  
- Create 2 files in the .aws directory `touch ~/.aws/config` and `touch ~/.aws/credentials`
- Create an IAM user to access your AWS Account. You can provide AdministratorAccess for testing purposes, but this user should be removed after use. To learn how to setup an IAM user, check [https://docs.aws.amazon.com/keyspaces/latest/devguide/access.credentials.html#access.credentials.IAM](here). Once you have created the user, configure your credentials and config file as explained [https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html](here).
- On your command line, export your profile with the command `export AWS_PROFILE=<name-of-your-profile>`.
- Create an S3 bucket on your AWS Console which will keep your state. Keep configurations with default values, such as NON public, S3 key for encryption. Don't enable versioning for now.

### Export a MySQL Username and Passord

Select a username and password to be used for the database and export them in your environment by running the following commands:

```
export TF_VAR_rds_secrets='{"username"="your_value_here", "password"="your_password_here"}'

```

Once you have completed the above, you are ready to start deploying!

*Please note:*

*Your terraform commands would typically be:*

*- `terraform init`*
*- `terraform plan`*
*- `terraform apply`*

### Create your VPC

First, please open the main.tf file and enter the name of the bucket you created to replace the string `<your_state_bucket>` and enter the name of your profile replacing the string `<your_profile_here>`.

Navigate to the `vpc` direcotry and run the above mentioned terraform commands. Please ensure that your run within the same command line environment within which you exported your `AWS_PROFILE`.


### Create your EC2

First, please open the main.tf file and enter the name of the bucket you created to replace the string `<your_state_bucket>` and enter the name of your profile replacing the string `<your_profile_here>`.

Navigate to the `basic-ec2-with-rds-access` directory and run your terraform commands.

### Create your lambda

First, please open the serverless.yml file and change the values quoted in <>:

- <Your_secret_arn_here> will be found on AWS Console -> Secrets Manager. It will be named `meetup-bastion`

- <your_db_url_here> will be found on AWS Console -> RDS and also in your terraform outputs after the `basic-ec2-with-rds-access` component has completed running.

- Please copy 3 subnet IDs to subsititute `<private_subnet_here>`. To find which subnets, go to AWS Console -> VPC and select the vpc called `devops-vpc`. Copy its ID and search for subnets associated with this VPC ID. Find the subnets with tags: `private = "true"`. Copy all 3 and substitute.

- Subsititute `<vpc_id_here>` with the VPC ID you just created.

Navigate to the `serverless-with-rds-access` directory and run:

- `sls plugin install -n serverless-python-requirements`
- `serverless deploy`


## Usage

There are 2 scripts made available under the `basic-ec2-with-rds-access` directory: `get-priv-key.sh` and `create-db.sql`. 

### Create Some Test Data

In order create some test data, you will need to hop onto the EC2 that's been setup by using its private key and IP address. You can view the IP address created by physically going on to your AWS Console and clicking on the EC2 that's been created. It would be called `bastion-meetup`.

Or you can navigate to `basic-ec2-with-rds-access` directory and run `terraform output`. The value with name `bastion_ip` would be the IP you're looking for. Please keep track of this IP. Please also keep track of the output named `rds_url`.

While you're in the `basic-ec2-with-rds-access`, run the following command:

`./get-priv-key.sh` 

This command will create a file in your current directory called `private.key`. Run the following command to ssh onto your EC2:

`ssh -i private.key ubuntu@IP_FROM_PREVIOUS_STEP` 

Once you're on the box, run the following commands:

- Install mysql: `sudo apt install mysql-client-core-8.0`
- Access the db: `mysql -u YOUR_USERNAME_FROM_PREVIOUS_STEP -h VALUE_OF_RDS_URL_FROM_PREVIOUS_STEP -p

_You will be prompted for the password and please enter your chosen database password._

- Copy paste the contents of the file `create_db.sql` and paste them on your command line. This should create a new table in your peachmeetup database.

### Test the API

Using [](Postman) or a curl command, you can test your API endpoint. To get your api endpoint, please go to your AWS Console, search for API Gateway and find the API gateway named 







```

```