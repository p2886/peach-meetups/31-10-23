resource "aws_iam_role" "ec2" {
  name = "${var.env}-${var.ec2_name}-ec2-assume-role"
  path = "/"

  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "ec2.amazonaws.com"
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy" "ec2" {
  name = "${var.env}-${var.ec2_name}-ec2-permissions"
  role = aws_iam_role.ec2.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ec2:*"
          ],
          "Resource" : "*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "secretsmanager:GetSecretValue"
          ],
          "Resource" : "${aws_secretsmanager_secret.rds.arn}"
        },
        
      ]
    }
  )
}

resource "aws_iam_instance_profile" "ec2" {
  name = "${var.env}-${var.ec2_name}-instance-profile"
  role = aws_iam_role.ec2.id
}
