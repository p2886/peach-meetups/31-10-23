resource "aws_db_instance" "this" {
  identifier             = "peach-meetup"
  allocated_storage      = 10
  db_name                = "peachmeetup"
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  username               = var.rds_secrets.username
  password               = var.rds_secrets.password
  parameter_group_name   = "default.mysql8.0"
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds.id]
  db_subnet_group_name   = aws_db_subnet_group.rds.id
}

resource "aws_db_subnet_group" "rds" {
  name       = "peach-meetup-rds-subnet-group"
  subnet_ids = [data.aws_subnet.private_1.id, data.aws_subnet.private_2.id, data.aws_subnet.private_3.id]
}
