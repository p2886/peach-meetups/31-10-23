variable "env" {
  default = "meetup"
}

variable "ec2_name" {
  default = "bastion"
}

variable "rds_secrets" {
  default = {
    username = ""
    password = ""
  }
  type = map(string)
}
