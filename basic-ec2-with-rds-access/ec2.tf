resource "aws_instance" "this" {
  ami                         = "ami-0563b16fda53ed3da"
  instance_type               = "t3.small"
  key_name                    = aws_key_pair.ec2.key_name
  vpc_security_group_ids      = [aws_security_group.ec2.id]
  iam_instance_profile        = aws_iam_instance_profile.ec2.name
  subnet_id                   = data.aws_subnet.public_1.id
  associate_public_ip_address = true
  monitoring                  = true

  root_block_device {
    delete_on_termination = true
    volume_size           = 8
  }

  tags = {
    created_by = "Grace Mukendi"
    Name = "${var.ec2_name}-${var.env}"
  }
}
