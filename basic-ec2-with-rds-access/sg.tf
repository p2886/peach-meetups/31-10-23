resource "aws_security_group" "ec2" {
  name        = "${var.env}-${var.ec2_name}-sg"
  description = "Allow HTTP inbound traffic"
  vpc_id      = data.aws_vpc.devops.id
}

resource "aws_security_group_rule" "tcp_ec2_ingress_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["Your_Public_IP_Here/32"]
  security_group_id = aws_security_group.ec2.id
}

resource "aws_security_group_rule" "tcp_ec2_egress_mysql" {
  type              = "egress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  cidr_blocks       = [data.aws_vpc.devops.cidr_block]
  security_group_id = aws_security_group.ec2.id
}

resource "aws_security_group_rule" "tcp_ec2_egress_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2.id
}

resource "aws_security_group_rule" "tcp_ec2_egress_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2.id
}

resource "aws_security_group" "rds" {
  name        = "${var.env}-rds-sg"
  description = "Allow inbound and outbound traffic"
  vpc_id      = data.aws_vpc.devops.id
}

resource "aws_security_group_rule" "tcp_rds_ingress_mysql" {
  type              = "ingress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  cidr_blocks       = [data.aws_vpc.devops.cidr_block]
  security_group_id = aws_security_group.rds.id
}
