data "aws_vpc" "devops" {
  tags = {
    "Name" = "devops-vpc"
  }
}

data "aws_subnet" "public_1" {
  vpc_id = data.aws_vpc.devops.id
  tags = {
    "Name" = "devops-public-1"
  }
}

data "aws_subnet" "public_2" {
  vpc_id = data.aws_vpc.devops.id
  tags = {
    "Name" = "devops-public-2"
  }
}

data "aws_subnet" "public_3" {
  vpc_id = data.aws_vpc.devops.id
  tags = {
    "Name" = "devops-public-3"
  }
}

data "aws_subnet" "private_1" {
  vpc_id = data.aws_vpc.devops.id
  tags = {
    "Name" = "devops-private-1"
  }
}

data "aws_subnet" "private_2" {
  vpc_id = data.aws_vpc.devops.id
  tags = {
    "Name" = "devops-private-2"
  }
}

data "aws_subnet" "private_3" {
  vpc_id = data.aws_vpc.devops.id
  tags = {
    "Name" = "devops-private-3"
  }
}
