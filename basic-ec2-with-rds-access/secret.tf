resource "random_string" "random" {
  length  = 3
  special = false
}

resource "aws_key_pair" "ec2" {
  key_name   = "${var.env}-${var.ec2_name}"
  public_key = tls_private_key.ec2.public_key_openssh
}

resource "tls_private_key" "ec2" {
  algorithm = "RSA"
}

resource "aws_secretsmanager_secret" "ec2" {
  name = "${var.env}-${var.ec2_name}-${random_string.random.id}.key"
}

resource "aws_secretsmanager_secret_version" "ec2" {
  secret_id     = aws_secretsmanager_secret.ec2.id
  secret_string = tls_private_key.ec2.private_key_pem
}

resource "aws_secretsmanager_secret" "rds" {
  name = "${var.env}-rds-${random_string.random.id}.creds"
}

resource "aws_secretsmanager_secret_version" "rds" {
  secret_id     = aws_secretsmanager_secret.rds.id
  secret_string = jsonencode(var.rds_secrets)
}