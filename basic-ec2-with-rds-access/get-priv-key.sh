#! /bin/bash

secret_arn=$(terraform output -raw ec2_secret_arn)
private_key=$(aws secretsmanager get-secret-value --secret-id "$secret_arn" --query SecretString --output text --region af-south-1)
echo "${private_key//'\n'/$'\n'}" >> private.key
chmod 400 private.key