output "ec2_secret_arn" {
  value = aws_secretsmanager_secret.ec2.arn
}

output "rds_secret_arn" {
  value = aws_secretsmanager_secret.rds.arn
}

output "bastion_ip" {
  value = aws_instance.this.public_ip
}

output "rds_url" {
  value = aws_db_instance.this.address
}

output "rds_db_name" {
  value = aws_db_instance.this.db_name
}