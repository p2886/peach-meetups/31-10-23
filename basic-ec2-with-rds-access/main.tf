provider "aws" {
  region  = "af-south-1"
  profile = "<your_profile_here>"
  default_tags {
    tags = {
      app        = "basic-ec2-with-rds-access"
      env        = "dev"
      managed_by = "terraform"
      git_url    = "https://gitlab.com/p2886/peach-meetups/31-10-23"
    }
  }
}

terraform {
  backend "s3" {
    bucket = "your_state_bucket_here"
    key    = "dev/af-south-1/basic-ec2-with-rds-access"
    region = "af-south-1"
  }
}
