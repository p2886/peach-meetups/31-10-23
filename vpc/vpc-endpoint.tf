resource "aws_vpc_endpoint" "this" {
  vpc_id              = module.vpc.vpc_id
  service_name        = "com.amazonaws.af-south-1.secretsmanager"
  private_dns_enabled = true
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.vpce.id]
  subnet_ids = module.vpc.private_subnets

  tags = {
    Name = "devops-vpce"
  }
}

resource "aws_security_group" "vpce" {
  name        = "devops-vpce-sg"
  description = "Allow HTTPS inbound and outbound traffic"
  vpc_id      = module.vpc.vpc_id
}

resource "aws_security_group_rule" "tcp_ec2_ingress_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["172.31.0.0/16"]
  security_group_id = aws_security_group.vpce.id
}

resource "aws_security_group_rule" "tcp_ec2_egress_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["172.31.0.0/16"]
  security_group_id = aws_security_group.vpce.id
}
