resource "aws_eip" "nat" {
  count = 3
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "devops-vpc"
  cidr = "172.31.0.0/16"

  azs                  = ["af-south-1a", "af-south-1b", "af-south-1c"]
  private_subnets      = ["172.31.64.0/20", "172.31.80.0/20", "172.31.96.0/20"]
  public_subnets       = ["172.31.48.0/20", "172.31.32.0/20", "172.31.16.0/20"]
  public_subnet_names  = ["devops-public-1", "devops-public-2", "devops-public-3"]
  private_subnet_names = ["devops-private-1", "devops-private-2", "devops-private-3"]

  enable_nat_gateway = true

  reuse_nat_ips       = true
  external_nat_ip_ids = aws_eip.nat.*.id

  create_igw = true

  enable_flow_log                      = true
  create_flow_log_cloudwatch_iam_role  = true
  create_flow_log_cloudwatch_log_group = true
  
  public_subnet_tags   = {
    public = "true"  
  }
  private_subnet_tags = {
    private = "true"
  }

  tags = {
    app        = "devops-vpc"
    env        = "dev"
    created_by = "Your Name here"
    managed_by = "terraform"
    git_url    = "https://gitlab.com/p2886/peach-meetups/31-10-23"
  }
}
