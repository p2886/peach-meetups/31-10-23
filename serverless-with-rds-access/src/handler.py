import json
import mysql.connector
import os
import boto3
from botocore.exceptions import ClientError

def register(event, context):

  print("Registing client..")
  
  SECRET_ARN = os.environ.get("DB_SECRET_ARN")
  secret = get_secret(SECRET_ARN)
  mydb = None 

  try:
    mydb = mysql.connector.connect(
      host=os.environ.get("DB_HOST"),
      database=os.environ.get("DB_NAME"),
      user=json.loads(secret)['username'],
      password=json.loads(secret)['password'],
      port=3306
    )

    print("Connection successful!")

    mycursor = mydb.cursor()
    sql = "INSERT INTO customer (customer_id, firstname, lastname) VALUES (null, %s, %s)"
    val = ("John", "Smith")
    mycursor.execute(sql, val)
    mydb.commit()

    response = {"statusCode": 200, "body": json.dumps("Customer registered.")}

  except Exception as e:
    print(f'Error connecting to DB ::: {e}')
  return response

def get_secret(secret_arn):
  client = boto3.client('secretsmanager')
  try:
    secret = client.get_secret_value(SecretId=secret_arn)
  except ClientError as e:
    print(f'Could not retrieve secret ::: {e}')
    raise e
  
  return secret['SecretString']

